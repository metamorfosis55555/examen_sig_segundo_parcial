<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/student','studentController');
Route::resource('/matter','matterController');
Route::resource('/teacher','teacherController');
Route::resource('/notes','notesController');

//Route:: get('notes/{id_notes}', 'notesController@show');
//Route:: post('notes/{id_notes}', 'notesController@suma');
<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $table='student';
    protected $primaryKey="id_student";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'CI',
        'name',
        'lastname',
        'mail',
        'address',
        'phone',
        'created_ad',
        'updated_ad'
    ];
}

<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class notes extends Model
{
    protected $table='notes';
    protected $primaryKey="id_notes";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'first_partial',
        'second_partial',
        'third_partial',
        'note_final',
        'id_teacher',
        'created_ad',
        'updated_ad'
    ];
    public function teacher()
    {
      return $this->belongsTo('app\model\teacher', 'id_teacher');
    }
}

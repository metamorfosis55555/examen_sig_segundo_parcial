<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class matter extends Model
{
    protected $table='matter';
    protected $primaryKey="id_matter";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'name_stuff',
        'career',
        'id_student',
        'created_ad',
        'updated_ad'
    ];
}

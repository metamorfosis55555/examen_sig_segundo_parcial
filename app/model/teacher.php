<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class teacher extends Model
{
    protected $table='teacher';
    protected $primaryKey="id_teacher";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'teacher_name',
        'specialty',
        'years_service',
        'id_matter',
        'created_ad',
        'updated_ad'
    ];
    public function notes()
    {
        return $this->hasOne('app\model\notes','id_teacher');
    }
}

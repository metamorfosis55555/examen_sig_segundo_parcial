<?php

namespace App\Http\Controllers;
use App\model\notes;
use Illuminate\Http\Request;

class notesController extends Controller
{
    
    public function destroy($id){
        $notes=notes::find($id);
        $notes->delete();
    }
    public function show($id){
        $notes=notes::find($id);
        return $notes;
    }
    public function update($id,Request $request){
        $notes=$this->show($id);
        $notes->fill($request->all())->save();
        return $notes;
    }



    public function store(Request $request){
        $notes = new notes($request->all());
        $cant=3;
        $notes->note_final=($notes->first_partial+$notes->second_partial+$notes->third_partial)/$cant;
        $notes->save();
        return $notes;
    }
    public function index(){
       $notes=notes::all();
       return view("teacher.lista",compact('teacher'));

    }
 
   
}

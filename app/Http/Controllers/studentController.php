<?php

namespace App\Http\Controllers;
use App\model\student;
use Illuminate\Http\Request;

class studentController extends Controller
{
    public function index(){
        return student::all();
    }
    public function store(Request $request){
        $student = new student();
        $student->fill($request->toArray())->save();
		return $student;
    }
    public function destroy($id){
        $student=student::find($id);
        $student->delete();
    }
    public function show($id){
        $student=student::find($id);
        return $student;
    }
    public function update($id,Request $request){
        $student=$this->show($id);
        $student->fill($request->all())->save();
        return $student;
    }
}

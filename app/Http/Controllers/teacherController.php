<?php

namespace App\Http\Controllers;
use App\model\teacher;
use Illuminate\Http\Request;

class teacherController extends Controller
{
    public function index(){
        return teacher::all();
    }
    public function store(Request $request){
        $teacher = new teacher();
        $teacher->fill($request->toArray())->save();
		return $teacher;
    }
    public function destroy($id){
        $teacher=teacher::find($id);
        $teacher->delete();
    }
    public function show($id){
        $teacher=teacher::find($id);
        return $teacher;
    }
    public function update($id,Request $request){
        $teacher=$this->show($id);
        $teacher->fill($request->all())->save();
        return $teacher;
    }
}

<?php

namespace App\Http\Controllers;
use App\model\matter;
use Illuminate\Http\Request;

class matterController extends Controller
{
    public function index(){
        return matter::all();
    }
    public function store(Request $request){
        $matter = new matter();
        $matter->fill($request->toArray())->save();
		return $matter;
    }
    public function destroy($id){
        $matter=matter::find($id);
        $matter->delete();
    }
    public function show($id){
        $matter=matter::find($id);
        return $matter;
    }
    public function update($id,Request $request){
        $matter=$this->show($id);
        $matter->fill($request->all())->save();
        return $matter;
    }
    
}
